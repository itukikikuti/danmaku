#include "XLibrary11.hpp"
using namespace XLibrary11;

struct Bullet
{
	Float3 position;
	Float3 speed;
	Float3 angles;
};

Bullet FireBullet(Float3 position, float angle, float speed)
{
	float radian = angle / 180.0f * 3.14f;
	Bullet bullet;
	bullet.position = position;
	bullet.speed = Float3(cosf(radian) * speed, sinf(radian) * speed, 0.0f);
	bullet.angles = Float3(0.0f, 0.0f, angle);

	return bullet;
}

int Main()
{
	Window::SetSize(320, 480);

	Camera camera;
	camera.color = Float4(0.0f, 0.0f, 0.0f, 1.0f);

	Sprite bullet1Sprite(L"bullet1.png");
	Sprite bullet2Sprite(L"bullet2.png");
	Sprite playerSprite(L"player.png");
	Sprite enemy1Sprite(L"enemy1.png");

	playerSprite.position.y = -200.0f;
	playerSprite.scale = Float3(2.0f, 2.0f, 2.0f);
	enemy1Sprite.scale = Float3(2.0f, 2.0f, 2.0f);

	int playerFireCount = 0;
	int enemyFireCount = 0;
	float enemyAngle = 0.0f;

	std::vector<Bullet> bullets1;
	std::vector<Bullet> bullets2;

	while (Refresh())
	{
		camera.Update();

		enemyFireCount++;

		if (enemyFireCount >= 60)
		{
			enemyFireCount = 0;
			for (float angle = 0.0f; angle < 360.0f; angle += 10.0f)
			{
				bullets2.push_back(FireBullet(enemy1Sprite.position, angle, 1.0f));
			}
		}

		enemyAngle += 1.0f;
		float radian = enemyAngle / 180.0f * 3.14f;
		enemy1Sprite.position = Float3(0.0f, 100.0f, 0.0f) + Float3(cosf(radian) * 100.0f, sinf(radian) * 100.0f, 0.0f);
		enemy1Sprite.Draw();

		if (Input::GetKey(VK_LEFT))
		{
			playerSprite.position.x -= 3.0f;
		}
		if (Input::GetKey(VK_RIGHT))
		{
			playerSprite.position.x += 3.0f;
		}
		if (Input::GetKey(VK_UP))
		{
			playerSprite.position.y += 3.0f;
		}
		if (Input::GetKey(VK_DOWN))
		{
			playerSprite.position.y -= 3.0f;
		}

		playerFireCount++;

		if (Input::GetKey('Z') && playerFireCount >= 5)
		{
			playerFireCount = 0;
			bullets1.push_back(FireBullet(playerSprite.position, 90.0f, 15.0f));
		}

		playerSprite.Draw();

		for (int i = 0; i < bullets1.size(); i++)
		{
			if (bullets1[i].position.x < -160.0f ||
				bullets1[i].position.x > 160.0f ||
				bullets1[i].position.y < -240.0f ||
				bullets1[i].position.y > 240.0f)
			{
				bullets1.erase(bullets1.begin() + i);
				i--;
				continue;
			}

			bullets1[i].position += bullets1[i].speed;
			bullet1Sprite.position = bullets1[i].position;
			bullet1Sprite.angles = bullets1[i].angles;
			bullet1Sprite.Draw();
		}

		for (int i = 0; i < bullets2.size(); i++)
		{
			if (bullets2[i].position.x < -160.0f ||
				bullets2[i].position.x > 160.0f ||
				bullets2[i].position.y < -240.0f ||
				bullets2[i].position.y > 240.0f)
			{
				bullets2.erase(bullets2.begin() + i);
				i--;
				continue;
			}

			bullets2[i].position += bullets2[i].speed;
			bullet2Sprite.position = bullets2[i].position;
			bullet2Sprite.angles = bullets2[i].angles;
			bullet2Sprite.Draw();
		}
	}

	return 0;
}
